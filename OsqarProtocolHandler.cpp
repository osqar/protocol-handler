#include "OsqarProtocolHandler.h"
//Global pointer to objects
OsqarHandler * PointerToOsqarHandler;

//Global objects
/*Send Recieve Osqar Protocol*/
RxManager SerialReciever;
TxManager SerialTransmiter;
/*End Of Send Recieve Osqar Protocol*/
/*Sampling Legs*/
ADCManager ADManager;
DifferentialADC ADCHip[4];
DifferentialADC ADCKnee[4];
/*End Of Sampling Legs*/
/*Protocol Samples*/
Sample FeedBackSample; //for test
Sample PWMSample1; //Part of protocol
Sample PWMSample2; //Part of protocol
Sample ADCSample1; //Part of Protocol
Sample ADCSample2; //Part of Protocol
/*End Of Protocol Samples*/

//global variables
uint8_t RecievedID=0;
uint8_t FeedBackData[8];
uint8_t * PWMData1[8]; //PWM of legs 1 and 2
uint8_t * PWMData2[8]; //PWM of legs 3 and 4
uint8_t * ADCData1[8]; //PWM of legs 1 and 2
uint8_t * ADCData2[8]; //PWM of legs 3 and 4

//for test
long LowDWord=0;
long HighDWord=0;
int StepCount=0;
float GaitVelocity=0.03; //default
uint8_t RecievedData[8];
long ReadTest=0;

void OsqarHandler::Initialize()
{
	
	//Initializing Quadruped and GaitHandler Objects
	Osqar.Initialize();
	TPD.Initialize(&Osqar);
	
	//Initializing Global objects
	FeedBackSample.Initialize();
	PWMSample1.Initialize(); //Samples of Hips and Knees PWM (Legs 1 and 2)
	PWMSample2.Initialize(); //Samples of Hips and Knees PWM (Legs 3 and 4)
	ADCSample1.Initialize(); //Samples of Hips and Knees Potentiometers (Legs 1 and 2)
	ADCSample2.Initialize(); //Samples of Hips and Knees Potentiometers (Legs 3 and 4)
	SerialReciever.Initialize();
	SerialTransmiter.Initialize();
	PointerToOsqarHandler=this;
	MainCallBack=this->MainDefault;
	ADManager.Initialize();
	
	for(int i=0;i<4;i++)
	{
		ADCHip[i].Attach(&ADManager);
		if(i!=3) //it only have 7 channels
			ADCKnee[i].Attach(&ADManager);
	}
	
	//Set SAMPLES:
	
	//Setting PWMSample
	//PWMData1
	PWMData1[0]=Osqar.Legs[0].ReadHipHAddress(); //Leg0.Hip High Byte
	PWMData1[1]=Osqar.Legs[0].ReadHipLAddress(); //Leg0.Hip Low Byte
	PWMData1[2]=Osqar.Legs[1].ReadHipHAddress(); //Leg1.Hip High Byte
	PWMData1[3]=Osqar.Legs[1].ReadHipLAddress();	//Leg1.Hip LowByte
	
	PWMData1[4]=Osqar.Legs[0].ReadKneeHAddress(); //Leg0.Knee High Byte
	PWMData1[5]=Osqar.Legs[0].ReadKneeLAddress(); //Leg0.Knee Low Byte
	PWMData1[6]=Osqar.Legs[1].ReadKneeHAddress(); //Leg1.Knee High Byte
	PWMData1[7]=Osqar.Legs[1].ReadKneeLAddress();//Leg1.Knee LowByte
 	//PWMData2
	PWMData2[0]=Osqar.Legs[2].ReadHipHAddress(); //Leg2.Hip High Byte
	PWMData2[1]=Osqar.Legs[2].ReadHipLAddress(); //Leg2.Hip Low Byte
	PWMData2[2]=Osqar.Legs[3].ReadHipHAddress(); //Leg3.Hip High Byte
	PWMData2[3]=Osqar.Legs[3].ReadHipLAddress(); //Leg3.Hip LowByte
	
	PWMData2[4]=Osqar.Legs[2].ReadKneeHAddress(); //Leg2.Knee High Byte
	PWMData2[5]=Osqar.Legs[2].ReadKneeLAddress(); //Leg2.Knee Low Byte
	PWMData2[6]=Osqar.Legs[3].ReadKneeHAddress(); //Leg3.Knee High Byte
	PWMData2[7]=Osqar.Legs[3].ReadKneeLAddress(); //Leg3.Knee LowByte	
	
	//Setting ADCSamples
	//ADCData1
	ADCData1[0]=ADCHip[0].ReadConversionByteHAddress();
	ADCData1[1]=ADCHip[0].ReadConversionByteLAddress();
	ADCData1[2]=ADCHip[1].ReadConversionByteHAddress();
	ADCData1[3]=ADCHip[1].ReadConversionByteLAddress();
	
	ADCData1[4]=ADCKnee[0].ReadConversionByteHAddress();
	ADCData1[5]=ADCKnee[0].ReadConversionByteLAddress();
	ADCData1[6]=ADCKnee[1].ReadConversionByteHAddress();
	ADCData1[7]=ADCKnee[1].ReadConversionByteLAddress();		
	
	//ADCData2
	ADCData2[0]=ADCHip[2].ReadConversionByteHAddress();
	ADCData2[1]=ADCHip[2].ReadConversionByteLAddress();
	ADCData2[2]=ADCHip[3].ReadConversionByteHAddress();
	ADCData2[3]=ADCHip[3].ReadConversionByteLAddress();
	
	ADCData2[4]=ADCKnee[2].ReadConversionByteHAddress();
	ADCData2[5]=ADCKnee[2].ReadConversionByteLAddress();
	//ADCData2[6]=ADCKnee[3].ReadConversionByteHAddress();
	//ADCData2[7]=ADCKnee[3].ReadConversionByteLAddress();		
	
	for(int i=0;i<SampleSize;i++)
	{
		FeedBackData[i]=0;
		RecievedData[i]=0;	
		//Protocol Samples Initializing
		PWMSample1.Add(PWMData1[i]);
		PWMSample2.Add(PWMData2[i]);
		ADCSample1.Add(ADCData1[i]);
		if(i<6)
		ADCSample2.Add(ADCData2[i]);
		//
	}
	
	SerialTransmiter.SubscribeSample(&PWMSample1); //Now will be transmitting this continiously
	SerialTransmiter.SubscribeSample(&PWMSample2); //Now will be transmitting this continiously
	SerialTransmiter.SubscribeSample(&ADCSample1); //Now will be transmitting this continiously
	SerialTransmiter.SubscribeSample(&ADCSample2); //Now will be transmitting this continiously
	//Initializing ProtocolSTM (Here is where all begins)
	  
	/*Main State*/
	DoWhenEnter=this->CheckIDAndDoMainCallBack;
	DoWhile=this->DoWhatProtocolSays;
	DoWhenOut=this->MainDefault;
	MainState.Create(&MainState,DoWhile,DoWhenEnter,DoWhenOut);	  
		
	//finally
	ProtocolSTM.Initialize(&MainState);
}

void OsqarHandler::Runner()
{	
	//Always sampling
	ADManager.Runner();
	
	//Always recieving
	SerialReciever.Runner();
	
	//Sends if has some stored data
	SerialTransmiter.Runner();
	
	//Runs protocol's statemachine
	ProtocolSTM.Runner();
}

/*MainState Functions*/
uint8_t OsqarHandler::MainDefault()
{
	//Does nothing
	return 0; //and stays in the same state
}

uint8_t OsqarHandler::CheckIDAndDoMainCallBack()
{
	if(SerialReciever.NewUnreadData())
	{
		//Update ReadTestValue
		//ReadTest=ADCHip[0].ReadConversion();
		
		RecievedID=SerialReciever.ReadID();
		HighDWord=SerialReciever.ReadHighData();
		LowDWord=SerialReciever.ReadLowData();
		for(int i=0;i<SampleSize;i++)
		{
			RecievedData[i]=SerialReciever.ReadByte(i);
		}
	}
	//RecievedID=Serial.parseInt();
	//LowDWord=Serial.parseInt();
//	if(PointerToOsqarHandler->ActualID!=RecievedID) //if new ID is recieved
//	{
		PointerToOsqarHandler->ActualID=RecievedID;	//update ActualID
		//Switch to case
		switch(PointerToOsqarHandler->ActualID)
		{
			case 250:
				PointerToOsqarHandler->DoWhenEnterToSBS();
				PointerToOsqarHandler->MainCallBack=PointerToOsqarHandler->StepByStepWalking;
			break;
			case 249:
				PointerToOsqarHandler->DoWhenEnterToCW();
				PointerToOsqarHandler->MainCallBack=PointerToOsqarHandler->ContiniouslyWalking;
			break;
			default: //stays with the same MainCallBack value
				int z=0;
		}
//	}
	return 0;//Ends WhenEnter
}

uint8_t OsqarHandler::DoWhatProtocolSays()
{
	PointerToOsqarHandler->MainCallBack();
	return 1; //allways does the transition
}
/**/

uint8_t OsqarHandler::DoWhenEnterToSBS()
{
	//does nothing
	//Update Values
	if(LowDWord==33)
	{
		PointerToOsqarHandler->TPD.Selector=1;//Set new step flag
		LowDWord=0;//clear next step flag
		
		long RecievedVelocity=0;
		RecievedVelocity=RecievedData[2];//HighData
		RecievedVelocity=(RecievedVelocity<<8)|RecievedData[3];//Entire Value
		
		GaitVelocity=(float)RecievedVelocity;
		//GaitVelocity=(GaitVelocity-MinRecieved)*(MaxVelocity-MinVelocity)/(MaxRecieved-MinRecieved)+MinVelocity;
		GaitVelocity=GaitVelocity/1000;
	}	
	return 0;
}

uint8_t OsqarHandler::StepByStepWalking()
{
	//Gait STM runner(Pause,Velocity)
	PointerToOsqarHandler->TPD.Runner(0,GaitVelocity);
	PointerToOsqarHandler->TPD.Selector=0;//clear new step flag	
	return 0;
}

uint8_t OsqarHandler::DoWhenEnterToCW()
{
	//Set Selector
	PointerToOsqarHandler->TPD.Selector=1;//clear new step flag		
	//Update Velocity		
	long RecievedVelocity=0;
	RecievedVelocity=RecievedData[2];//HighData
	RecievedVelocity=(RecievedVelocity<<8)|RecievedData[3];//Entire Value
	
	GaitVelocity=(float)RecievedVelocity;
	//GaitVelocity=(GaitVelocity-MinRecieved)*(MaxVelocity-MinVelocity)/(MaxRecieved-MinRecieved)+MinVelocity;
	GaitVelocity=GaitVelocity/1000;

	return 0;
}

uint8_t OsqarHandler::ContiniouslyWalking()
{
	bool Pause=1;
	if(LowDWord==33)
		Pause=0;//Set to Running (Isn't implemented)
	
	//Gait STM runner(Pause,Velocity)
	PointerToOsqarHandler->TPD.Runner(0,GaitVelocity);
	return 0;
}
