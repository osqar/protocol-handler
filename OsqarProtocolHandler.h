#ifndef OsqarProtocolHandler_h
#define OsqarProtocolHandler_h

#include <OsqarSerialProtocol.h>
#include <QuadrupedGait.h>
#include <StateMachine.h>
#include <BaBauHound.h>
#include <LegMovementManager.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>

/*
	This object, gets ID and DataBytes from OsqarSerialProtocol as an input for it's state machine.
*/
class OsqarHandler;

class OsqarHandler
{
	public:
		void Initialize();
		void Runner();
	private:
		/*Osqar (because there's only one Osqar)*/
			Quadruped Osqar;
		/*Osqar =)*/
		/*QuadrupedGait Section*/
			
			//ther could be a method that Adds QuadrupedGaits, asigning them ID's selectors
			//Using New and Delete opperators maybe
			TwoPhasesDiscontinious TPD;
		
		/*End of QuadrupedGaits Section*/
		
		uint8_t ActualID;
		
		/*ProtocolHandler State Machine*/
			//StateMachine
			StateMachine ProtocolSTM; //Starts in main state
			
			/******************************************/
			/*				STATES
			/******************************************/
			//MainState
			State MainState;
			
			/*MainState functions*/
				static uint8_t MainDefault(); //stays in main until ID has a valid value
				//WhenEnterFunction
				static uint8_t CheckIDAndDoMainCallBack(); //MainCallBack points to function that should be doing
				//Doing
				static uint8_t DoWhatProtocolSays();
				//does default when out (for now)				
			/*End Of MainState functions*/
					
			/*SettingWalkingLimits*/
				static uint8_t DoWhenEnterToSWL();
				static uint8_t SettingWalkingLimits();
			/*SettingPositionLegOffset*/
				static uint8_t DoWhenEnterToSPLO();
				static uint8_t SettingPositionLegOffset();
			/*SettingAngleLegOffset*/	
				static uint8_t DoWhenEnterToSALO();
				static uint8_t SettingAngleLegOffset();
			/*MovingByLegPosition*/
				static uint8_t DoWhenEnterToMBLP();
				static uint8_t MovingByLegPosition();
			/*MovingByLegAngle*/
				static uint8_t DoWhenEnterToMBA();
				static uint8_t MovingByAngle();
			/*ContiniouslyWalking*/
				static uint8_t DoWhenEnterToCW();
				static uint8_t ContiniouslyWalking();
			/*StepByStepWalking functions*/			
				static uint8_t DoWhenEnterToSBS();
				static uint8_t StepByStepWalking();
			/*End Of StepByStepWalking functions*/
			//PointerToFunctions used to initialize states
			PointerToFunction DoWhenEnter;
			PointerToFunction DoWhile;
			PointerToFunction DoWhenOut;
			PointerToFunction MainCallBack;
			
		/*End Of Protocol Handler State Machine*/
};

#endif

